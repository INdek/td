CREATE TABLE tv_show (
	id          INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	airdates_id INTEGER UNIQUE,

	-- Can be null because we can get an airdates id and not know the show name.
	name        VARCHAR,

	is_followed BOOLEAN NOT NULL DEFAULT 0
);

CREATE TABLE season (
	id         INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	tv_show_id INTEGER NOT NULL,
	number     INTEGER NOT NULL CHECK (number >= 0),

	UNIQUE(tv_show_id, number)
		ON CONFLICT REPLACE,
	FOREIGN KEY(tv_show_id) REFERENCES tv_show(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);

CREATE TABLE episode (
	id           INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
	title        VARCHAR,
	number       INTEGER CHECK (number >= 0),
	season_id    INTEGER NOT NULL,
	watched      BOOLEAN NOT NULL DEFAULT 0,
	release_date DATE    NOT NULL,

	UNIQUE(season_id, number)
		ON CONFLICT REPLACE,
	FOREIGN KEY(season_id) REFERENCES season(id)
		ON DELETE CASCADE
		ON UPDATE CASCADE
);
