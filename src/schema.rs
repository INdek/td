/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

// TODO: This isn't great. fix it
//infer_schema!("/home/afonso/git/td/test.db");

table! {
    episodes (id) {
        id -> Integer,
        title -> Nullable<Text>,
        number -> Nullable<Integer>,
        release_date -> Date,
        season_id -> Integer,
    }
}

table! {
    seasons (id) {
        id -> Integer,
        number -> Integer,
        tv_show_id -> Integer,
    }
}

table! {
    tv_shows (id) {
        id -> Integer,
        airdates_id -> Nullable<Integer>,
        name -> Text,
        is_followed -> Bool,
    }
}

joinable!(episodes -> seasons (season_id));
