/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use {reqwest, kuchiki};
use kuchiki::traits::TendrilSink;
use kuchiki::NodeRef;
use chrono::NaiveDate;
use url::Url;
use basic_episode::BasicEpisode;

use ErrorKind::*;
use ::*;
use models::*;

// TODO: At some point we might want to remove all database access from here
// and move it elsewhere

lazy_static! {
    static ref BASE_URL: Url = Url::parse("http://www.airdates.tv/").unwrap();
    static ref LIST_SERIES_URL: Url = Url::parse("http://www.airdates.tv/_u/listSeries/").unwrap();
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct DayEntry {
    pub title: String,
    pub series_id: u32,
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Day {
    pub date: NaiveDate,
    pub entries: Vec<DayEntry>,
}

fn fetch_day_date(day_node: &NodeRef) -> Result<NaiveDate> {
    let date_b = day_node.as_element()?.attributes.borrow();

    let date_string = date_b.map.get(&qn!("", "data-date")).ok_or_else(|| {
        InvalidParse("No data-date".into())
    })?;


    let year: i32 = date_string.get(0..4)?.parse()?;
    let month: u32 = date_string.get(4..6)?.parse()?;
    let day: u32 = date_string.get(6..8)?.parse()?;

    Ok(NaiveDate::from_ymd(year, month, day))
}

fn parse_entry(entry_node: &NodeRef) -> Result<DayEntry> {
    let entry_node_element = entry_node.as_element()?;
    let entry_node_b = entry_node_element.attributes.borrow();

    let series_id: u32 = entry_node_b
        .map
        .get(&qn!("", "data-series-id"))
        .ok_or_else(|| InvalidParse("No data-series-id".into()))?
        .parse()?;


    let title = entry_node.select(".title")?.nth(0)?.text_contents();

    Ok(DayEntry { title, series_id })
}

pub fn fetch_shows() -> Result<Vec<Day>> {
    let mut resp = reqwest::get(BASE_URL.clone())?;
    // TODO remove the assert
    assert!(resp.status().is_success());

    let content = resp.text()?;
    let document = kuchiki::parse_html().one(content);
    let document_matches = document
        .select(".days > .day")
        .map_err(|()| SelectError("Error selecting days".into()))?;


    document_matches
        .map(|css_match| {
            let day_node = css_match.as_node();
            let date = fetch_day_date(day_node)?;

            let entries: Result<Vec<DayEntry>> = day_node
                .select(".entry")
                .map_err(|()| SelectError("Error selecting days".into()))?
                .map(|entry_node| parse_entry(entry_node.as_node()))
                .collect();


            Ok(Day {
                date,
                entries: entries?,
            })
        })
        .collect()
}

impl Day {
    pub fn insert_db(&self, conn: &mut DbConn) -> Result<()> {
        let tx = conn.transaction()?;

        for day_entry in &self.entries {
            let basic_episode = BasicEpisode::from_string(&day_entry.title)?;
            let airdates_id = day_entry.series_id as i32;
            tx.prepare(r"
                INSERT OR IGNORE
                INTO tv_show (airdates_id, name)
                VALUES (?, ?);
            ")?.execute(&[&airdates_id, &basic_episode.name])?;


            tx.prepare(r"
                INSERT OR IGNORE
                INTO season (tv_show_id, number)
                VALUES ((SELECT id FROM tv_show WHERE airdates_id = ?), ?);
            ")?.execute(&[&airdates_id, &basic_episode.season])?;

            tx.prepare(r"
                INSERT OR IGNORE
                INTO episode (season_id, number, release_date)
                VALUES (
                    (SELECT season.id
                     FROM season
                     JOIN tv_show ON season.tv_show_id = tv_show.id
                     WHERE tv_show.airdates_id = ? AND season.number = ?),
                ?, ?);
            ")?.execute(&[&airdates_id, &basic_episode.season, &basic_episode.episode, &self.date])?;
        }

        tx.commit()?;
        Ok(())
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Deserialize)]
pub struct FollowedShow {
    pub id: i32,
    pub color: String,
}

pub fn fetch_follows<S: AsRef<str>>(username: S) -> Result<Vec<FollowedShow>> {
    let mut resp = reqwest::get(LIST_SERIES_URL.join(username.as_ref())?)?;
    // TODO remove the assert
    assert!(resp.status().is_success());

    let content = resp.text()?;
    Ok(serde_json::from_str(&content)?)
}

impl FollowedShow {
    pub fn insert_db(&self, conn: &DbConn) -> Result<()> {
        TvShow::follow_by_acestream_id(conn, self.id)
    }
}
