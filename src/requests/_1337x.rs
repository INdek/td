/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use {reqwest, kuchiki};
use kuchiki::traits::TendrilSink;
use url::Url;
use regex::Regex;

use ErrorKind::*;
use ::*;

lazy_static! {
    static ref BASE_URL: Url = Url::parse("http://1337x.to/").unwrap();
}

fn create_search_url<S: Into<String>>(search: S) -> Result<Url> {
    lazy_static! {
        static ref URL_CLEAN_REGEX: Regex = Regex::new(r"[:]+").unwrap();
    }
    // TODO: at some point we should build something more robust
    let search = URL_CLEAN_REGEX.replace_all(&search.into(), "")
        .replace(" ", "+");

    Ok(BASE_URL
       .clone()
       .join("search/")?
       .join(&format!("{}/", search))?
       .join("1/")?)
}

pub fn fetch_torrents<S: Into<String>>(search: S) -> Result<Vec<IncompleteTorrent>> {
    let url = create_search_url(search)?;
    let mut resp = reqwest::get(url)?;
    // TODO remove the assert
    assert!(resp.status().is_success());

    let content = resp.text()?;
    let document = kuchiki::parse_html().one(content);
    let result_rows = document
        .select(".table-list > tbody > tr")
        .map_err(|()| SelectError("Error selecting result rows".into()))?;

    let torrents: Vec<IncompleteTorrent> = result_rows
        .map(|css_match| {
            let node = css_match.as_node();
            let row_text = node.text_contents();

            let title_node = node.select(".name > a:not(.icon)")?.nth(0)?;

            let node_borrow = title_node.attributes.borrow();
            let row_href: &str = node_borrow.map.get(&qn!("", "href")).ok_or_else(|| InvalidParse(
                "No href on title"
                    .into(),
            ))?;

            let url = BASE_URL.join(row_href)?;

            Ok(IncompleteTorrent::parse(row_text, url)?)
        })
        .collect::<Result<_>>()?;

    Ok(torrents)
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IncompleteTorrent {
    pub title: String,
    pub seeders: u32,
    pub leechers: u32,
    /// Url to fetch the rest of the torrent
    pub full_url: Url,
}

impl TorrentInfo for IncompleteTorrent {
    fn title(&self) -> String {
        self.title.clone()
    }
    fn seeders(&self) -> u32 {
        self.seeders
    }
    fn leechers(&self) -> u32 {
        self.leechers
    }
}

impl IncompleteTorrent {
    pub fn parse<S: AsRef<str>>(input: S, full_url: Url) -> Result<IncompleteTorrent> {
        lazy_static! {
            static ref TITLE_REGEX: Regex = Regex::new(r"^(.*?)(?:\d*)?$").unwrap();
        }
        let input: Vec<&str> = input.as_ref()
            .split('\n')
            .map(|s| s.trim())
            .collect();

        let title: String = input
            .get(1)
            .and_then(|raw_title| TITLE_REGEX.captures(raw_title).unwrap().get(1))
            .map(|title| title.as_str().to_owned())
            .ok_or_else(|| InvalidParse("No title found".into()))?;

        let seeders: u32 = input.get(2).map(|input| input.parse::<u32>()).ok_or_else(
            || {
                InvalidParse("No seeders found".into())
            },
        )??;

        let leechers: u32 = input.get(3).map(|input| input.parse::<u32>()).ok_or_else(
            || {
                InvalidParse("No leechers found".into())
            },
        )??;

        Ok(IncompleteTorrent {
            title,
            seeders,
            leechers,
            full_url,
        })

    }

    pub fn get_full_torrent(self) -> Result<Torrent> {
        let mut resp = reqwest::get(self.full_url.clone())?;
        // TODO remove the assert
        assert!(resp.status().is_success());

        let content = resp.text()?;
        let document = kuchiki::parse_html().one(content);

        let magnet_node = document
            .select(".download-links-dontblock > li > a")
            .map_err(|()| SelectError("Error selecting magnet".into()))?
            .nth(0)?;

        let node_borrow = magnet_node.attributes.borrow();
        let magnet_link: &str = node_borrow.map.get(&qn!("", "href")).ok_or_else(|| {
            InvalidParse("No href on title".into())
        })?;

        let url = Url::parse(magnet_link)?;

        Ok(Torrent::from_incomplete_torrent(self, url))
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Torrent {
    pub title: String,
    pub seeders: u32,
    pub leechers: u32,
    /// Magnet link
    pub magnet: Url,
}

impl Torrent {
    pub fn from_incomplete_torrent(ic: IncompleteTorrent, magnet: Url) -> Torrent {
        Torrent {
            title: ic.title,
            seeders: ic.seeders,
            leechers: ic.leechers,
            magnet,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use url::Url;

    #[test]
    fn parse_incomplete_torrent_with_comments() {
        let url = Url::parse("http://test.test/").unwrap();
        assert_eq!(
            IncompleteTorrent::parse(
                r"
Marvels.Inhumans.S01E05.HDTV.x264-KILLERS[eztv]2
2923
332
Oct. 21st '17
350.0 MB2923
SeekNDstroy

            ",
                url.clone(),
            ).unwrap(),
            IncompleteTorrent {
                title: "Marvels.Inhumans.S01E05.HDTV.x264-KILLERS[eztv]".into(),
                seeders: 2923,
                leechers: 332,
                full_url: url,
            }
        )
    }

    #[test]
    fn parse_incomplete_torrent() {
        let url = Url::parse("http://test.test/").unwrap();
        assert_eq!(
            IncompleteTorrent::parse(
                r"
Marvels.Inhumans.S01E02.WEB-DL.x264-RARBG
1020
108
Sep. 30th '17
339.9 MB1020
SeekNDstroy

            ",
                url.clone(),
            ).unwrap(),
            IncompleteTorrent {
                title: "Marvels.Inhumans.S01E02.WEB-DL.x264-RARBG".into(),
                seeders: 1020,
                leechers: 108,
                full_url: url,
            }
        )
    }

    #[test]
    fn create_url() {
        assert_eq!(
            create_search_url("test").unwrap(),
            Url::parse("http://1337x.to/search/test/1/").unwrap()
        );
    }

    #[test]
    fn create_url_space() {
        assert_eq!(
            create_search_url("test test").unwrap(),
            Url::parse("http://1337x.to/search/test+test/1/").unwrap()
        );
    }
}
