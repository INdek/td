/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/


#![cfg_attr(feature="clippy", feature(plugin))]
#![cfg_attr(feature="clippy", plugin(clippy))]


#![feature(try_trait)]

#![deny(missing_debug_implementations)]
#![allow(dead_code)]

#![recursion_limit="1024"]

#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate log;
extern crate env_logger;
extern crate itertools;
#[macro_use]
extern crate lazy_static;
extern crate regex;
extern crate url;


#[macro_use]
extern crate clap;
extern crate app_dirs;
extern crate config as _config;

extern crate chrono;
extern crate rusqlite;

extern crate open;

extern crate reqwest;
extern crate kuchiki;
extern crate html5ever;
extern crate serde;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;


#[macro_use]
mod macros;
mod requests;
mod config;
mod rating;
mod models;
mod commands;
mod basic_episode;

use config::Config;
use models::*;
use std::option::NoneError;
use clap::App;

lazy_static! {
	static ref CONFIG: Config = Config::new().expect("Failed to load config");
}

error_chain! {
    links {
        Config(config::Error, config::ErrorKind);
    }
    foreign_links {
        Io(::std::io::Error);
        LogInit(::log::SetLoggerError);
        AppDirs(::app_dirs::AppDirsError);
        InvalidIntParse(::std::num::ParseIntError);
        ReqError(::reqwest::Error);
        UrlParseError(::url::ParseError);
        JsonError(::serde_json::Error);
        RusqliteError(::rusqlite::Error);
    }
    errors {
        InvalidParse(s: String) {
            description("Invalid parse")
            display("An error occured when parsing the data: '{}'", s)
        }
        SelectError(s: String) {
            description("Invalid select")
            display("An error occured when selecting an html node: '{}'", s)
        }
        UnkownError {
            description("Unkown error")
            display("Unkown error")
        }
        NoneError(e: NoneError) {
            description("Expected a value and got none")
            display("Expected a value and got none")
        }
    }
}

impl From<()> for self::Error {
    fn from(_error: ()) -> Self {
        Self::from_kind(ErrorKind::UnkownError)
    }
}

impl From<NoneError> for self::Error {
    fn from(error: NoneError) -> Self {
        Self::from_kind(ErrorKind::NoneError(error))
    }
}

type DbConn = rusqlite::Connection;

pub trait TorrentInfo {
    fn title(&self) -> String;
    fn seeders(&self) -> u32;
    fn leechers(&self) -> u32;
}

fn main() {
    env_logger::init();

    let yaml = load_yaml!("../conf/cli.yml");
    let matches = App::from_yaml(yaml).get_matches();
    let conn = CONFIG.connect_db().expect("Failed to open database");

    commands::launch(conn, matches).expect("An error ocurred");
}
