/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use app_dirs::{AppInfo, AppDataType, get_app_root};
use _config;
use std::env;
use rusqlite::Connection;

error_chain! {
    foreign_links {
        Io(::std::io::Error);
        AppDirs(::app_dirs::AppDirsError);
        RusqliteError(::rusqlite::Error);
        ConfigError(_config::ConfigError);
    }
}


#[derive(Debug, Clone)]
pub struct Config {
    pub app_info: AppInfo,
    pub config: _config::Config,
}

impl Config {
    pub fn new() -> Result<Config> {
        // TODO: Use default from Cargo.toml
        let mut local_conf = Config {
            app_info: AppInfo {
                name: env!("CARGO_PKG_NAME"),
                author: "Afonso Bordado",
            },
            config:_config::Config::default(),
        };

        local_conf.load()?;

        Ok(local_conf)
    }

    fn apply_db_config(&self, conn: &mut Connection) -> Result<()> {
        // Enable foreign keys
        conn.execute("PRAGMA foreign_keys = ON;", &[])?;

        let logging: bool = self.config.get("database.logging").unwrap_or(false);
        if logging {
            conn.trace(Some(|sql| debug!("SQL: {}", sql.trim())));
        }

        Ok(())
    }

    fn load(&mut self) -> Result<()> {
        let app_config = get_app_root(AppDataType::UserConfig, &self.app_info).map(
            |mut path| {
                path.push("config.toml");
                path
            },
        )?;

        info!("Loading default config");
        info!("Loading config file: {}", app_config.to_str().expect("Failed to read path"));

        self.config
            .merge(_config::File::from_str(
                include_str!("../conf/default_conf.toml"),
                _config::FileFormat::Toml,
            ))?
            .merge(_config::File::from(app_config))?;

        Ok(())
    }

    pub fn connect_db(&self) -> Result<Connection> {
        // TODO: The default database path
        // Should not be the curr dir
        let default_database_path = env::current_dir().map(|mut p| {
            p.push("database.db");
            p
        })?;

        let database_path: String = self.config
            .get("database.path")
            .unwrap_or_else(|_| default_database_path.to_str().unwrap().into());

        let in_memory: bool = self.config
            .get("database.in_memory")
            .unwrap_or(false);

        info!("Using DB: {}", if in_memory { "(in memory)" } else { &database_path } );

        let mut conn = if in_memory {
            Connection::open_in_memory()?
        } else {
            Connection::open(database_path)?
        };
        self.apply_db_schema(&mut conn)?;
        self.apply_db_config(&mut conn)?;

        Ok(conn)
    }

    fn apply_db_schema(&self, conn: &mut Connection) -> Result<()> {
        let table_count: i64 = conn.query_row(r#"
            SELECT count(name) FROM sqlite_master WHERE type='table';
        "#, &[], |r| r.get(0))?;

        if table_count == 0 {
            info!("Database has no tables. Applying schema");
            conn.execute_batch(include_str!("database.sql"))?;
        }

        Ok(())
    }
}
