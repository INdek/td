/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use std::cmp::{Ord, Ordering};
use TorrentInfo;

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Rating<T>(pub T, pub i32);

impl<T: Eq> Ord for Rating<T> {
    fn cmp(&self, other: &Self) -> Ordering {
        other.1.cmp(&self.1)
    }
}

impl<T: Eq> PartialOrd for Rating<T> {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(other.1.cmp(&self.1))
    }
}

impl<T: TorrentInfo> Rating<T> {
    pub fn rate<R: AsRef<str>>(source: T, search_term: R) -> Rating<T> {
        let mut rating: i32 = 0;
        let lowercase_title = source.title().to_lowercase();
        let search_term = search_term.as_ref();

        rating += rate_quality(&lowercase_title);
        rating += rate_keywords(&lowercase_title);
        rating += rate_search_term(&lowercase_title, search_term);
        rating += rate_seeders(source.seeders(), source.leechers());

        Rating(source, rating)
    }
}

fn rate_seeders(seeders: u32, leechers: u32) -> i32 {
    let seed_score = match seeders {
        0 => -100,
        1...10 => -40,
        11...100 => -20,
        101...2000 => 20,
        x if x > 2000 => 40,
        _ => panic!("Error grading seeders"),
    };

    let leech_score = match leechers {
        0...10 => 1,
        11...100 => 3,
        x if x > 100 => 5,
        _ => panic!("Error grading leechers"),
    };

    let leechers = if leechers == 0 { 1 } else { leechers };
    let sl_ratio = seeders as f32 / leechers as f32;
    let multiplier = if sl_ratio < 1.0 { -1.0 } else { 1.0 };
    let trunc_score = sl_ratio.trunc() * 2.0;
    let fract_score = sl_ratio.fract() * 2.0;
    let ratio_score = (trunc_score + fract_score) * multiplier;

    seed_score + leech_score + ratio_score.round() as i32
}

fn rate_search_term<S: AsRef<str>>(lowercase_title: S, search_term: &str) -> i32 {
    let title = lowercase_title.as_ref();
    let search_term = search_term.to_lowercase();

    let search_words = search_term.split(' ');
    let rating: i32 = search_words
        .map(|word| title.find(word).map_or(-1, |_| 0))
        .sum();

    rating * 40
}

fn rate_quality<S: AsRef<str>>(title: S) -> i32 {
    let title = title.as_ref();
    let mut rating: i32 = 0;
    rating += title.find("1080p").map_or(0, |_| 80);
    rating += title.find("720p").map_or(0, |_| 20);
    rating += title.find("480p").map_or(0, |_| -20);
    rating += title.find("xvid").map_or(0, |_| -10);
    rating
}


fn rate_keywords<S: AsRef<str>>(title: S) -> i32 {
    let title = title.as_ref();
    let mut rating: i32 = 0;
    rating += title.find("eztv").map_or(0, |_| 10);
    rating += title.find("rarbg").map_or(0, |_| 10);
    rating += title.find("afg").map_or(0, |_| -10);
    rating
}
