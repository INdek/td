/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

#![allow(unused_macros)]

macro_rules! qn {
    ($ns: tt, $local: tt) => (
        ::html5ever::QualName::new(None, $ns.into(), $local.into())
    )
}

macro_rules! squery {
    ($( $qb: tt )*) => { {
        use ::diesel::debug_query;
        use ::diesel::sqlite::Sqlite;

        debug!("{}", debug_query::<Sqlite, _>(&$($qb)*));
        $($qb)*
    } }
}
