/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

mod config;
mod fetch;
mod follows;

use ::{DbConn, Result};
use clap::ArgMatches;

pub fn launch(conn: DbConn, matches: ArgMatches) -> Result<()> {

    // You can also match on a subcommand's name
    match matches.subcommand_name() {
        Some("config")       => config::launch(conn, matches),
        Some("follows")      => follows::launch(conn, matches),
        Some("fetch") | None => fetch::launch(conn, matches),
        _                    => panic!("Unrecognized command!"),
    }?;

    Ok(())
}
