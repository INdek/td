/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use ::*;
use rating::Rating;
use basic_episode::BasicEpisode;
use clap::ArgMatches;
use ::requests::_1337x::IncompleteTorrent;

fn update_database_shows(mut conn: &mut DbConn) -> Result<()> {
    let days = requests::airdates::fetch_shows()?;

    info!("Fetched {} days of info", days.len());
    info!("{} entries", days.iter()
          .map(|day| day.entries.len())
          .sum::<usize>());


    let res: Result<Vec<()>> = days
        .into_iter()
        .map(|day| Ok(day.insert_db(&mut conn)?))
        .collect();

    let _ = res?;
    Ok(())
}

/// Gets episodes that are in a followed tv show and have
/// been released for at least a day
fn get_unwatched_episodes(conn: &mut DbConn) -> Result<Vec<(Id, String, i32, i32)>> {
    let mut stmt = conn.prepare(r"
        SELECT
            episode.id,
            tv_show.name,
            season.number,
            episode.number
        FROM episode
        JOIN season ON season.id = episode.season_id
        JOIN tv_show ON tv_show.id = season.tv_show_id
        WHERE
            tv_show.is_followed = 1 AND
            episode.release_date < DATE('now') AND
            episode.watched = 0
        ORDER BY
            tv_show.id,
            season.number ASC,
            episode.number ASC;
    ")?;

    let rows = stmt.query_map(&[], |row| {(
        row.get(0),
        row.get(1),
        row.get(2),
        row.get(3),
    )})?;

    let mut ids = Vec::new();
    for row in rows {
        ids.push(row?);
    }

    Ok(ids)
}

fn print_new_episodes_summary(episodes: &Vec<(Id, BasicEpisode)>) {
    println!("Found {} new episodes", episodes.len());

    for (id, episode) in episodes {
        println!("\t{}", episode);
    }
}

fn fetch_episode(episode: &BasicEpisode) -> Result<()> {
    let torrents = requests::_1337x::fetch_torrents(episode.search_term())?;

    let mut rated_torrents = torrents
        .into_iter()
        .map(|source| Rating::rate(source, episode.search_term()))
        .collect::<Vec<Rating<IncompleteTorrent>>>();

    rated_torrents.sort();

    // rated_torrents.iter().for_each(|rt| {
    //     let torrent = &rt.0;
    //     let rating = rt.1;
    //     println!("{} - {}", rating, torrent.title)
    // });

    let full_torrent = rated_torrents.into_iter().nth(0)?.0.get_full_torrent()?;

    open::that(full_torrent.magnet.into_string())?;

    Ok(())
}

pub fn launch(mut conn: DbConn, _matches: ArgMatches) -> Result<()> {
    update_database_shows(&mut conn)?;

    let episodes = get_unwatched_episodes(&mut conn)?.iter()
        .map(|data| (data.0, BasicEpisode::new(data.1.clone(), data.2, data.3)))
        .collect();

    print_new_episodes_summary(&episodes);

    for (id, episode) in episodes {
        match fetch_episode(&episode) {
            Ok(_) => {
                conn.prepare(r"
                    UPDATE episode SET watched = 1 WHERE id = ?;
                ")?.execute(&[&id])?;
            },
            Err(_) => println!("Failed: {}", episode),
        };
    }

    Ok(())
}
