/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use ::*;
use clap::ArgMatches;

fn get_local_follows(conn: &mut DbConn) -> Result<Vec<i32>> {
    let mut stmt = conn.prepare("
        SELECT airdates_id FROM tv_show WHERE is_followed = 1;
    ")?;
    let rows = stmt.query_map(&[], |row| -> i32 {
        row.get(0)
    })?;
    let local_follows = rows
        .fold(Ok(vec![]), |acc: std::result::Result<Vec<i32>, rusqlite::Error>, val| {
            let mut acc = acc?;
            acc.push(val?);
            Ok(acc)
        });

    Ok(local_follows?)
}

struct FollowChange {
    pub unchanged: Vec<i32>,
    pub new: Vec<i32>,
    pub removed: Vec<i32>,
}

impl FollowChange {
    pub fn diff(local: &Vec<i32>, remote: &Vec<i32>) -> Self {
        let removed: Vec<i32> = local.iter()
            .filter(|l| remote.iter().find(|r| l == r).is_none())
            .map(|l| *l)
            .collect();

        let new: Vec<i32> = remote.iter()
            .filter(|r| local.iter().find(|l| r == l).is_none())
            .map(|l| *l)
            .collect();

        let unchanged: Vec<i32> = remote.iter()
            .filter(|r| local.iter().find(|l| r == l).is_some())
            .map(|l| *l)
            .collect();


        FollowChange {
            unchanged,
            new,
            removed
        }
    }
}

fn print_show_list(conn: &mut DbConn, list: &Vec<i32>) {
    let mut show_name_stmt = conn.prepare("
        SELECT name FROM tv_show WHERE airdates_id = ?1;
    ").expect("Failed to prepare query");

    for show_id in list {
        let show_name: std::result::Result<String, _> = show_name_stmt
            .query_row(&[show_id], |row| row.get(0));

        print!("\t");
        match show_name {
            Ok(name) => println!("{}", name),
            Err(rusqlite::Error::QueryReturnedNoRows) => println!("Unkown show id: {}", show_id),
            Err(e) => println!("Failed to execute query: {:?}", e),
        }
    }

}

fn print_diff(conn: &mut DbConn, diff: &FollowChange) {
    println!("New follows:");
    print_show_list(conn, &diff.new);
    println!("Unfollowed:");
    print_show_list(conn, &diff.removed);
}

fn apply_db_changes(conn: &mut DbConn, diff: &FollowChange) {
    let mut update_stmt = conn.prepare("
        INSERT OR REPLACE INTO tv_show(airdates_id, is_followed, name)
        VALUES (
        	?1,
        	?2,
        	(SELECT name FROM tv_show WHERE airdates_id = ?1)
        );
    ").expect("Failed to prepare query");

    for new_id in &diff.new {
        update_stmt
            .execute(&[new_id, &1])
            .expect("Failed to execute query");
    }

    for unfollowed_id in &diff.removed {
        update_stmt
            .execute(&[unfollowed_id, &0])
            .expect("Failed to execute query");
    }
}

pub fn launch(mut conn: DbConn, _matches: ArgMatches) -> Result<()> {
    let follows: Vec<i32> = requests::airdates::fetch_follows("OilPictureWolfPot")?
        .iter()
        .map(|f| f.id)
        .collect();
    let follow_size = follows.len();

    let local_follows = get_local_follows(&mut conn)?;

    let diff = FollowChange::diff(&local_follows, &follows);

    print_diff(&mut conn, &diff);

    apply_db_changes(&mut conn, &diff);

    println!("\nFollowing {} shows", follow_size);

    Ok(())
}
