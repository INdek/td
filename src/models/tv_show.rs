/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use ::*;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct TvShow {
    pub id: Id,
    pub airdates_id: Option<Id>,
    pub name: String,
    pub is_followed: bool,
}

impl TvShow {
    /// Finds a TvShow by name
    // TODO: Needs some cleanup
    pub fn find<T: Into<String>>(conn: &DbConn, search_name: T) -> Result<Option<TvShow>> {
        let mut stmt = conn.prepare(r"
            SELECT id, airdates_id, name, is_followed FROM tv_show WHERE name = ?;
        ")?;
        let mut result = stmt.query(&[&search_name.into()])?;

        let tv_show = if let Some(row) = result.next() {
            let row = row?;
            Some(TvShow {
                id: row.get(0),
                airdates_id: row.get(1),
                name: row.get(2),
                is_followed: row.get(3),
            })
        } else {
            None
        };

        Ok(tv_show)
    }

    pub fn follow_by_acestream_id(conn: &DbConn, air_id: i32) -> Result<()> {
        let mut stmt = conn.prepare(r"
            UPDATE tv_show SET is_followed = 1 WHERE airdates_id = ?
        ")?;
        stmt.execute(&[&air_id])?;

        Ok(())
    }
}
