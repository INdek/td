/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use super::*;
use ::*;

#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Season {
    pub id: Id,
    pub number: i32,
    pub tv_show_id: Id,
}

impl Season {
    pub fn find_by_id(conn: &DbConn, id: Id) -> Result<Option<Season>> {
        Ok(conn.query_row(
            r"SELECT id, number, tv_show_id FROM season WHERE id = ?;",
            &[&id],
            |row| Some(Season {
                id: row.get(0),
                number: row.get(1),
                tv_show_id: row.get(2),
            })
        )?)
    }
}
