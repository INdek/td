/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use chrono::NaiveDate;
use super::*;

// TODO: Add release date to ep
#[derive(Clone, Debug, PartialEq, Eq)]
pub struct Episode {
    pub id: Id,
    pub title: Option<String>,
    pub number: Option<i32>,
    pub release_date: NaiveDate,
    pub season_id: Id,
}

impl Episode {
}
