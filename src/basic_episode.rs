/*
 * Copyright (C) the td contributors
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 only,
 * as published by the Free Software Foundation.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 **/

use std::fmt;
use regex::{Regex, Match, Captures};
use ::Result;

#[derive(Debug, PartialEq)]
pub struct BasicEpisode {
    pub name: String,
    pub season: i32,
    pub episode: i32
}

impl BasicEpisode {
    pub fn new<S: Into<String>>(name: S, season: i32, episode: i32) -> Self {
        Self {
            name: name.into(),
            season,
            episode
        }
    }

    pub fn from_string<S: AsRef<str>>(title: S) -> Result<Self> {
        lazy_static! {
            static ref DATA_REGEX: Regex = Regex::new(r"^(.*?)(?:S(\d+))?(?:E(\d+))?$").unwrap();
        }
        let captures: Captures = DATA_REGEX.captures(title.as_ref())?;

        let title_res: Result<Match> = captures.get(1).ok_or_else(|| "Missing title".into());


        let title: &str = title_res.map(|m| m.as_str().trim())?;

        let season_str: &str = captures.get(2).map_or("00", |m| m.as_str());

        let episode_str: &str = captures.get(3).map_or("00", |m| m.as_str());

        let season = season_str.parse::<i32>()?;
        let episode = episode_str.parse::<i32>()?;
        Ok(BasicEpisode::new(title, season, episode))
    }

    pub fn search_term(&self) -> String {
        format!("{} S{:02}E{:02}", self.name, self.season, self.episode)
    }
}

impl fmt::Display for BasicEpisode {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", self.search_term())
    }
}



#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn format() {
        assert_eq!("The Flash S04E05", format!("{}", BasicEpisode::new("The Flash", 4, 5)));
        assert_eq!("The Flash S04E05", BasicEpisode::new("The Flash", 4, 5).search_term());
    }

    #[test]
    fn parse_normal() {
        let entry = BasicEpisode::from_string("The Flash S04E05").unwrap();
        assert_eq!(entry, BasicEpisode::new("The Flash", 4, 5));
    }

    #[test]
    fn parse_missing_season() {
        let entry = BasicEpisode::from_string("Wild Kratts E10").unwrap();
        assert_eq!(entry, BasicEpisode::new("Wild Kratts", 0, 10));
    }

    #[test]
    fn parse_missing_ep() {
        let entry = BasicEpisode::from_string("test tv showhh").unwrap();
        assert_eq!(entry, BasicEpisode::new("test tv showhh", 0, 0));
    }
}
